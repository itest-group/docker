#bash
mkdir -p ../../backups
exec &>> ../../backups_log

find ../../backups/ -type f ! -newermt "$(date --date="1 day ago")" -exec rm {} \;

mkdir -p ../../backups
docker container exec $(docker ps --format "{{.Names}}" | grep testserver_test_redmine_mysql.1)  sh -c 'MYSQL_PWD="'$(cat ../../secrets/mysql/mysql_redmine_root_password)'" exec mysqldump -uroot redmine' | gzip > ../../backups/$(date +%Y-%m-%d_%H:%M:%S).sql.gz

