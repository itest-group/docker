# Docker.
## Requirements.

Linux. 

Docker 20.10.2+. 

Docker compose 1.25.0+.

## Install soft.
```
    sudo apt update && apt upgrade
    sudo apt install docker docker.io containerd docker-compose git zip
```

## Get docker.
```
    mkdir itest-group && cd itest-group
    git@gitlab.com:itest-group/docker.git
```

##	Production environment: before the first launch.
```
    cd itest-group
    
    mkdir -p secrets/ssl/{main,redmine,bot,bot-api}

    cp /ssl/main/fullchain.pem secrets/ssl/main/fullchain.pem
    cp /ssl/main/privkey.pem secrets/ssl/main/privkey.pem

    cp /ssl/redmine/fullchain.pem secrets/ssl/redmine/fullchain.pem
    cp /ssl/redmine/privkey.pem secrets/ssl/redmine/privkey.pem

    cp /ssl/bot/fullchain.pem secrets/ssl/bot/fullchain.pem
    cp /ssl/bot/privkey.pem secrets/ssl/bot/privkey.pem

    cp /ssl/bot-api/fullchain.pem secrets/ssl/bot-api/fullchain.pem
    cp /ssl/bot-api/privkey.pem secrets/ssl/bot-api/privkey.pem
    
    cp /ssl/bot/fullchain.pem secrets/main/ssl/bot/fullchain.pem
    cp /ssl/bot/privkey.pem secrets/main/ssl/bot/privkey.pem
    
    cp /.htpasswd secrets/.htpasswd
    
    mkdir -p secrets/mysql
    echo 'password' > secrets/mysql/mysql_root_password
    
    sudo docker-compose -f stack.yml run test_redmine_mysql
    
    sudo docker container exec -it container_id mysql -uroot -p
    CREATE USER 'redmine'@'%' IDENTIFIED BY 'password';
    GRANT ALL ON redmine.* TO 'redmine'@'%';
    exit;
    
    cd ..
    sudo docker exec -i container_id sh -c 'exec mysql -uredmine -p"password" redmine' < dump.sql
    sudo docker container stop container_id
```

##	Production deploy.
```
    cd docker
    sudo docker-compose -f build.yml build
    sudo docker stack deploy -c stack.yml testserver
```

## Production update.
```
    sudo docker service update --force --image test_proxy_nginx:latest testserver_test_proxy_nginx
```

##	Production compose (test mode).
```
    cd docker
    sudo docker-compose -f stack.yml up
```

## Configure db backups.

insert to crontab "*/30 * * * * cd /var/www/itest-group/docker/scripts/ && bash backup.bash"
